﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using XML_Locker.CTC;

namespace SAPToCommon
{
    public class ConvertToCommon
    {
        public string fileNameToConvert { get; set; }
        public string targetSchema{ get; set; }
        public ConvertToCommon()
        {

        }

        public ConvertToCommon(string fileName, string XSD)
        {
            fileNameToConvert = fileName;
            targetSchema = XSD;
        }

        public NodeFile ReturnCommon()
        {
            NodeFile result = new NodeFile();
            DELVRY03 sapDel = new DELVRY03();
            sapDel = GetSapObject(fileNameToConvert, targetSchema);
            FileInfo processingFile = new FileInfo(fileNameToConvert);
            DELVRY03 sapFile = new DELVRY03();
            result = ConvertSapToCommon(sapDel);

            return result;
        }

        private NodeFile ConvertSapToCommon(DELVRY03 sapDel)
        {
            throw new NotImplementedException();
        }

        private DELVRY03 GetSapObject(string fileNameToConvert, string targetSchema)
        {
            DELVRY03 sapDel = new DELVRY03();
            FileInfo processingFile = new FileInfo(fileNameToConvert);
            DELVRY03 sapFile = new DELVRY03();
            using (FileStream fStream = new FileStream(fileNameToConvert, FileMode.Open))
            {
                XmlSerializer cwConvert = new XmlSerializer(typeof(DELVRY03));
                sapDel = (DELVRY03)cwConvert.Deserialize(fStream);
                fStream.Close();
            }
            return sapDel;
        }
    }
}
